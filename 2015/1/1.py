import os
from functools import reduce

if __name__ == "__main__":
    # First part
    with open('input', 'r') as instructions:
        next = lambda a, e: a + (1 if e == '(' else -1)
        print(sum(map(lambda i: reduce(next, i, 0), instructions)))

    # Second part
    with open('input', 'r') as instructions:
        floor = 0
        index = 1
        for moves in instructions:
            for move in moves:
                floor = next(floor, move)
                if floor < 0: break
                else: index += 1
        
        print(index)
