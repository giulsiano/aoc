import os
from functools import reduce
from concurrent.futures import ThreadPoolExecutor


# 2*l*w + 2*w*h + 2*h*l

if __name__ == "__main__":
    # First part
    with ThreadPoolExecutor(max_workers=len(os.sched_getaffinity(0))) as e:
        to_sides_dim = lambda s: [int(c) for c in s.split('x')]
        with open('input', 'r') as boxes:
            area = lambda w, h, l: 2*(l*w + w*h + h*l) + min([l*w, w*h, h*l])
            dimensions = [e.submit(to_sides_dim, box) for box in boxes]
            areas = [e.submit(area, *dimension.result()) for dimension in dimensions]
            print(sum([area.result() for area in areas]))
                
        # Second part
        def sort(list):
            list.sort()
            return list

        # Compute ribbon which is (l*w*h) + 2*(sum of littlest sides)
        with open('input', 'r') as boxes:
            ribbon = lambda l,w,h: 2*(sum(sort([l,w,h])[:-1])) + l*w*h
            # Parse text dimensions to integer
            dimensions = [e.submit(to_sides_dim, box) for box in boxes]
            ribbons = [e.submit(ribbon, *dimension.result()) for dimension in dimensions]
            print(sum([ribbon.result() for ribbon in ribbons]))
        
