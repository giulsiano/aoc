import concurrent.futures
import hashlib
import multiprocessing

prefix = 6 * "0"

if __name__ == "__main__":
    inp = None
    with open("input", "r") as salt:
        inp = salt.read().encode('ascii')

    def isPrefixed(i):
        num = str(i).encode('ascii')
        return num if hashlib.md5(inp + num).hexdigest().startswith(prefix) else None

    def lenProd(queue, len):
        start = 0
        end = len
        while(True):
            queue.put((start, end))
            start = end
            end += len

    queue = multiprocessing.Queue(10000)
    producer = multiprocessing.Process(target=lenProd, args=(queue,10000))
    with concurrent.futures.ProcessPoolExecutor() as ex:
        result = 0
        producer.start()
        while(not result):
            for found in ex.map(isPrefixed, range(*queue.get())):
                result = found if found else 0
                if result: break
        
        print(result)
        producer.terminate()
        queue.close()

