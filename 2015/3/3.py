commands = {
            'v': ( 0, -1),
            'V': ( 0, -1),
            '^': ( 0,  1),
            '<': (-1,  0),
            '>': ( 1,  0)
        }

def visitHouses (moves):
    position = (0,0)
    houses = {position}
    for move in moves:
        position = (commands[move][0] + position[0], commands[move][1] + position[1])
        houses.add(position)

    return houses


if __name__ == "__main__":
    with open("input", "r") as moves:
        print(len(visitHouses(moves.read())))

    # Second part
    with open("input", "r") as f:
        indexedMoves = list(enumerate(f.read()))
        odd = tuple(move for (idx, move) in indexedMoves if idx & 1)
        even = tuple(move for (idx, move) in indexedMoves if not idx & 1)

        robo = visitHouses(odd)
        santa = visitHouses(even)

        print(len(robo | santa))
        
