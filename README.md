# Advent Of Code Series

This repository contains all the code written for completing each task on [Advent of Code](https://adventofcode.com/).
It is separated by years and for now the only language I have used to solve each problem is Python. As time passes I
should add separate directories for each language with their own input (if the input from AoC has changed).

These are essentially toy programs, the philosophy I chose is "make it the best you can do" so I tried to apply different
solutions, such as using executors, futures or simply doing things sequentially, in order for me to have more understanding
of each language. 

[[_TOC_]]

## 2015

There are a lot of problems regarding SantaClaus and is the first try for me to write and solve AoC problems using only
my own programming skills. I hope you enjoy reading the code, but I mainly hope you can actually read my code without too
much problems. If you encounter some difficulties in reading my code make me know that by doing whatever you want.
